package com.example.kjankiewicz.android_03w03;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // setup action bar for tabs
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
            /*
             * Stworzenie zakładek dla każdego fragmentu
             * i przypisanie im odbiornika TabListener
             * Android-03w-interfejs-cz1: slajd 36
             */
            ActionBar.Tab tab = actionBar.newTab()
                    .setText(R.string.main)
                    .setTabListener(new TabListener<>(
                            this, "Main", MainFragment.class));
            actionBar.addTab(tab);

            tab = actionBar.newTab()
                    .setText(R.string.second)
                    .setTabListener(new TabListener<>(
                            this, "Second", SecondFragment.class));
            actionBar.addTab(tab);

            tab = actionBar.newTab()
                    .setText(R.string.third)
                    .setTabListener(new TabListener<>(
                            this, "Third", ThirdFragment.class));
            actionBar.addTab(tab);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    /*
     * Obsługa zakładek za pomocą obdiornika TabListener
     * Android-03w-interfejs-cz1: slajd 37
     */
    public static class TabListener<T extends Fragment> implements ActionBar.TabListener {
        private Fragment mFragment;
        private final Activity mActivity;
        private final String mTag;
        private final Class<T> mClass;

        /**
         * Constructor used each time a new tab is created.
         *
         * @param activity The host Activity, used to instantiate the fragment
         * @param tag      The identifier tag for the fragment
         * @param clz      The fragment's Class, used to instantiate the fragment
         */
        TabListener(Activity activity, String tag, Class<T> clz) {
            mActivity = activity;
            mTag = tag;
            mClass = clz;
        }

        /*
         * Obsługa zaznaczenia zakładki
         * Android-03w-interfejs-cz1: slajd 37
         */
        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
            // Check if the fragment is already initialized
            if (mFragment == null) {
                // If not, instantiate and add it to the activity
                mFragment = Fragment.instantiate(mActivity, mClass.getName());
                ft.add(android.R.id.content, mFragment, mTag);
            } else {
                // If it exists, simply attach it in order to show it
                ft.attach(mFragment);
            }
        }

        /*
         * Obsługa odzaznaczenia zakładki
         * Android-03w-interfejs-cz1: slajd 37
         */
        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
            if (mFragment != null) {
                // Detach the fragment, because another one is being attached
                ft.detach(mFragment);
            }
        }

        /*
         * Obsługa ponownego zaznaczenia, wcześniej już zaznaczonej zakładki
         * Android-03w-interfejs-cz1: slajd 37
         */
        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
            // User selected the already
            // selected tab. Usually do
            // nothing.
        }
    }
}
