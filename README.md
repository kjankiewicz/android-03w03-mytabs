Tytuł: **Wykorzystanie zakładek do nawigacji**

Zaprezentowane zagadnienia:

* Nawigacja z użyciem zakładek

Projekt prezentuje wykorzystanie zakładek umieszczonych w pasku akcji do nawigowania pomiędzy fragmentami, dynamicznie podmienianymi w głównej aktywności.

Najważniejsze pliki:

* *MainActivity.java* - główna aktywność aplikacji zawiera kod tworzący zakładki w pasku akcji oraz implementuje odbiornik *TabListener*, służący do obsługi operacji zaznaczania wybranej zakładki i odznaczenia poprzedniej aktywnej zakładki. Wynikiem tych operacji jest podmienienie fragmentu, reprezentującego główny widok aplikacji.